<?php
$nextStart = $start + $rows;
$nextUrl = '#';
$nextClass = 'next disabled';
if ($nextStart < $numFound) {
  $nextUrl = 'search.php?q='.$query.'&start='.$nextStart.'&rows='.$rows;
  $nextClass = 'next';
}

$prevStart = $start - $rows;
$prevUrl = '#';
$prevClass = 'previous disabled';
if ($prevStart >= 0) {
  $prevUrl = 'search.php?q='.$query.'&start='.$prevStart.'&rows='.$rows;
  $prevClass = 'previous';
}
?>
<div class="row">
  <ul class="pager">
    <li class="<?=$prevClass?>">
      <a href="<?=$prevUrl?>">&larr; Prev</a>
    </li>
    <li class="<?=$nextClass?>">
      <a href="<?=$nextUrl?>">Next &rarr;</a>
    </li>
  </ul>
</div>
