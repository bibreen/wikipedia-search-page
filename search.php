<?
  require_once('common.php');
  $query = isset($_GET['q']) ? $_GET['q'] : '';
  $start = isset($_GET['start']) ? $_GET['start'] : 0;
  $rows = isset($_GET['rows']) ? $_GET['rows'] : $NUM_ROWS;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Solr/은전한닢을 사용한 한국어 위키백과 검색</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container-fluid">
    <?php
    include("search_bar.php");
    if ($query) {
      print("<div class=\"span7\">\n");
      include("search_result.php");
      include("search_pager.php");
      print("</div>\n");
    } else { ?>
      <div class="span7">
        <div class="row">
          <p>
            <ul>
              <li>이 페이지는 <a href="http://eunjeon.blogspot.kr/2013/02/blog-post.html"><strong>은전한닢 형태소 분석기</strong></a>를 테스트하기 위한 위키백과 검색 페이지입니다.</li>
              <li>mecab-ko-dic-1.2.0-20130427, mecab-ko-lucene-analyzer-0.9.5가 사용되었습니다. </li>
              <li>한국어 검색의 특징상 루씬의 하이라이팅 모듈이 한국어 검색에서 문제점을 가지고 있습니다. 감안해서 봐주세요.</li>
              <li><a href="https://bitbucket.org/bibreen/wikipedia-search-page/src/master/search_functions.php">여기</a>서 사용된 검색식을 보실 수 있습니다. 루씬 파서를 사용했으며, 대충 짠 소스니 이것도 감안해서 봐주세요. :)</li>
              <li><a href="http://twitter.github.com/bootstrap/">트위터 부트스트랩</a> 겁나 좋은 놈이군요. :)</li>
            </ul>
          </p>
        </div>
      </div>
    <?php
    }
    ?>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
