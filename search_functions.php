<?php
function search($collection, $query, $start, $rows) {
  $escapedQuery = escape($query);
  $encodedSolrQuery = makeSolrQuery($escapedQuery);
  $serializedResult = file_get_contents(
      'http://localhost:8983/solr/'.$collection.
      '/search?q='.$encodedSolrQuery.
      '&start='.$start.
      '&rows='.$rows.
      '&hl.q='.makeHighlightQuery($escapedQuery).
      '&wt=phps&indent=true');
  //print($serializedResult);
  return $serializedResult;
}

function makeSolrQuery($query) {
  $solrQuery =
    '{!lucene q.op=AND}'.
    '"'.$query.'" OR '.
    'title_exact:('.$query.')^30.0 OR '.
    'title_t:"'.$query.'"~10^15.0 OR '.
    'synonyms_txt:"'.$query.'"~10^10.0 OR '.
    'text_t:"'.$query.'"~5 OR '.
    'text_t:('.$query.')';
  return urlencode($solrQuery);
}

function makeHighlightQuery($query) {
  $highlightQuery = '"'.$query.'" '.$query;
  return urlencode($highlightQuery);
}

function escape($string)
{
  $match = array(
    '\\', '+', '-', '&', '|', '!', '(', ')', '{', '}',
    '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
  $replace = array(
    '\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}',
    '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
  $string = str_replace($match, $replace, $string);
  return $string;
}
?>
