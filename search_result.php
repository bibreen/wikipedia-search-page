<?php
require_once('search_functions.php');
$serializedResult = search('collection1', $query, $start, $rows);
$result = unserialize($serializedResult);

$numFound = $result['response']['numFound'];
$page = ($start / $rows) + 1;
$qtime = $result['responseHeader']['QTime'];
?>
<div class="row">
  <p class="text-right">위키검색 총 <?=$numFound?> 개 중 <?=$page?> 페이지 (<?=$qtime?> ms)</p>
</div>
<!--div class="row"></div-->
<?php
foreach ($result['response']['docs'] as $doc) {
//print_r($doc);
$id = $doc['id'];
$title = $doc['title_t'];
$score = $doc['score'];

$url = 'http://ko.wikipedia.org/wiki/' . str_replace(' ', '_', $title);
$highlightedTitle = $result['highlighting'][$id]['title_t'][0];
$highlightedSynonym = '';
if (isset($result['highlighting'][$id]['synonyms_txt'])) {
  $highlightedSynonym = $result['highlighting'][$id]['synonyms_txt'][0];
}
$snippet = implode('...', $result['highlighting'][$id]['text_t']);
?>
<div class="row">
<a href="<?=$url?>" target="_blank"><?=$highlightedTitle?></a>
<?php if (!empty($highlightedSynonym)) { ?>
  &nbsp; &nbsp;<small class="muted">[<?=$highlightedSynonym?>]</small>
<?php } ?>
&nbsp; &nbsp;<small class="muted">(score: <?=$score?>)</small>
</div>
<div class="row">
<p><?=$snippet?></p>
</div>
<div class="control-group"></div>
<?php
}
?>
